# coding: utf-8
################################----IMPORTS----################################
import keras
import numpy as np
import os
import glob

import time
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import Dropout
from sklearn import preprocessing
from keras.utils import np_utils
from keras.callbacks import ModelCheckpoint
from keras.utils.visualize_util import plot
import matplotlib.pyplot as plt
import pickle
import shutil
import sys

################################----END OF IMPORTS----################################


################################----DEFINITIONS----################################


def is_experiment_name_not_valid(experiment):
    return experiment != 'sgda' and \
           experiment != 'sgdad' and \
           experiment != 'sgdn' and \
           experiment != 'sgdnd' and \
           experiment != 'sgd' and \
           experiment != 'sgdd'


def validate_input():
    if len(sys.argv) < 2:
        str = "Not enough arguments. You should provide model name. For example: \n" \
              "python train.py sgda"
        sys.exit(str)
    else:
        if is_experiment_name_not_valid(sys.argv[1]):
            sys.exit("Wrong experiment name. Should be one of the following: \n"
                     "sgda, sgdad, sgdn, sgdnd, sgd, sgdd")

start_time = time.time()
model_directory_path = 'out/model'  # + str(time.strftime("_%Y_%m_%d_%H_%M_%S"))
dirlist = glob.glob('classification/data/tctodd?')
model_weights_filename = model_directory_path + "/weights.best.hdf5"
model_visualization_fielname = model_directory_path + "/model.png"
json_model_fielaname = model_directory_path + '/model_settings.json'
label_encoder_file_path = model_directory_path + "/label_encoder"
model_accuracy_plot_file_name = model_directory_path + '/model_accuracy.png'
model_loss_plot_file_name = model_directory_path + '/model_loss.png'

validate_input()

experiment = sys.argv[1]  # sgda, sgdad, sgdn, sgdnd, sgdd, sgd

def adjust_input_to_fixed_length(fixed_length, array):
    for i in range(len(array)):
        tmp = np.zeros((fixed_length, 22))
        tmp[-array[i].shape[0]:] = array[i]
        array[i] = tmp
    return array


################################----END OF DEFINITIONS----################################


################################----LOGGING SETTINGS----################################

'''
oldStdout = sys.stdout
logfile = open('logFile', 'w')
sys.stdout = logfile
'''

################################----END OF LOGGING SETTINGS----################################


shutil.rmtree(model_directory_path)
os.makedirs(model_directory_path)

X = []
Y = []

for dir_ in dirlist:
    for file_ in os.listdir(dir_):
        Y.append(file_.split('.')[0][:-2])
        X.append(np.genfromtxt(dir_ + '/' + file_, delimiter='\t'))

np.random.seed(7)

max_length = max([x.shape[0] for x in X])
split_one = int(len(X) * 0.6)
split_two = int(len(X) * 0.8)

X_train = X[:split_one]
Y_train = Y[:split_one]
X_validation = X[split_one:split_two]
Y_validation = Y[split_one:split_two]
X_test = X[split_two:]
Y_test = Y[split_two:]

X_train = adjust_input_to_fixed_length(max_length, X_train)
X_validation = adjust_input_to_fixed_length(max_length, X_validation)
X_test = adjust_input_to_fixed_length(max_length, X_test)

X_train = np.array(X_train)
X_test = np.array(X_test)
X_validation = np.array(X_validation)

print X_train.shape, X_test.shape, X_validation.shape

le = preprocessing.LabelEncoder()
le.fit(Y_train)
label_encoder_file = open(label_encoder_file_path, 'wb')
pickle.dump(le, label_encoder_file, pickle.HIGHEST_PROTOCOL)
label_encoder_file.close()
Y_train = np_utils.to_categorical(le.transform(Y_train))
Y_test = np_utils.to_categorical(le.transform(Y_test))
Y_validation = np_utils.to_categorical(le.transform(Y_validation))

checkpoint = ModelCheckpoint(model_weights_filename, monitor='val_acc', verbose=1, save_best_only=True, mode='max')
callbacks_list = [checkpoint]

model = Sequential()
batch_size = 32
epochs = 2000
model.add(LSTM(100, input_shape=(max_length, 22)))

if experiment == 'sgdad' or experiment == 'sgdnd' or experiment == 'sgdd':
    model.add(Dropout(0.2))

model.add(Dense(len(set(Y)), activation='softmax'))

if experiment == 'sgda' or experiment == 'sgdad':
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
elif experiment == 'sgdn' or experiment == 'sgdnd':
    sgd = keras.optimizers.SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
    model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy'])
elif experiment == 'sgd' or experiment == 'sgdd':
    model.compile(loss='categorical_crossentropy', optimizer='sgd', metrics=['accuracy'])

print(model.summary())
json = model.to_json()

f = open(json_model_fielaname, 'w')
f.write(json)
f.close()

history = model.fit(X_train, Y_train, validation_data=(X_validation, Y_validation), callbacks=callbacks_list,
                    nb_epoch=epochs, batch_size=batch_size)
scores = model.evaluate(X_test, Y_test, verbose=0)
print("Accuracy: %.2f%%" % (scores[1] * 100))

# summarize history for accuracy
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
fig1 = plt.gcf()
fig1.savefig(model_accuracy_plot_file_name)
# summarize history for loss

plt.clf()
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
fig2 = plt.gcf()
fig2.savefig(model_loss_plot_file_name)

plot(model, to_file=model_visualization_fielname)
elapsed_time = time.time() - start_time
print elapsed_time
# logfile.close()
