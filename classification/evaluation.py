from keras.models import model_from_json
import numpy as np
import pickle
import json
import glob
import os, sys
from keras.utils import np_utils


def validate_input():
    if len(sys.argv) < 2:
        str = "Not enough arguments. You should provide model directory. For example: \n" \
              "python evaluation.py models/sgd"
        sys.exit(str)


def adjust_input_to_fixed_length(fixed_length, array):
    for i in range(len(array)):
        tmp = np.zeros((fixed_length, 22))
        tmp[-array[i].shape[0]:] = array[i]
        array[i] = tmp
    return array


validate_input()
model_directory_path = sys.argv[1]

model_weights_filename = model_directory_path + "/weights.best.hdf5"
model_visualization_fielname = model_directory_path + "/model.png"
json_model_fielaname=model_directory_path + '/model_settings.json'
label_encoder_file_path =model_directory_path + "/label_encoder"
dirlist = glob.glob('data/tctodd?')

json_file = open(json_model_fielaname,'r')
data = json.load(json_file)
max_length = int(data["config"][0]["config"]["batch_input_shape"][1])
json_file.close()

json_file = open(json_model_fielaname,'r')
json_string = json_file.read()
model = model_from_json(json_string)
json_file.close()

model.load_weights(model_weights_filename)
model.compile(loss='categorical_crossentropy', optimizer='adam',metrics=['accuracy'])

le = open(label_encoder_file_path, 'rb')
label_encoder = pickle.load(le)
le.close()

X = []
Y = []

for dir_ in dirlist:
    for file_ in os.listdir(dir_):
        Y.append(file_.split('.')[0][:-2])
        X.append(np.genfromtxt(dir_ + '/' + file_, delimiter='\t'))

np.random.seed(7)

split_one = int(len(X) * 0.6)
split_two = int(len(X) * 0.8)


X_test = X[split_two:]
Y_test = Y[split_two:]

X_test = adjust_input_to_fixed_length(max_length,X_test)
X_test = np.array(X_test)

Y_test = np_utils.to_categorical(label_encoder.transform(Y_test))
scores = model.evaluate(X_test, Y_test, verbose=0)
print model.metrics_names
print("Accuracy: %.2f%%" % (scores[1]*100))