from keras.models import model_from_json
import numpy as np
import pickle
import json
import sys
import os.path

#================================DEFINITIONS=========================================


def validate_input():
    if len(sys.argv) < 3:
        str = "Not enough arguments. You should provide model directory and input file path. For example: \n" \
              "python classify_file.py models/sgda data/tctodd1/alive-1.tsd"
        sys.exit(str)
    else:
        if not os.path.isdir(sys.argv[1]):
            sys.exit("Wrong model directory")
        elif not os.path.isfile(sys.argv[2]):
            sys.exit("the file " + sys.argv[2] + " doesn't exist")


def adjust_input_to_fixed_length(fixed_length, array):
    for i in range(len(array)):
        tmp = np.zeros((fixed_length, 22))
        tmp[-array[i].shape[0]:] = array[i]
        array[i] = tmp
    return array

#=================================END OF DEFINITIONS=========================================


validate_input()

experiment_name = sys.argv[1]
test_file = sys.argv[2]

model_directory_path = experiment_name

model_weights_filename = model_directory_path + "/weights.best.hdf5"

json_model_fielaname=model_directory_path + '/model_settings.json'

label_encoder_file_path = model_directory_path + "/label_encoder"

json_file = open(json_model_fielaname,'r')

data = json.load(json_file)

max_length = int(data["config"][0]["config"]["batch_input_shape"][1])

json_file.close()

json_file = open(json_model_fielaname,'r')
json_string = json_file.read()
model = model_from_json(json_string)
json_file.close()

model.load_weights(model_weights_filename)

label_encoder_file = open(label_encoder_file_path,'rb')
label_encoder = pickle.load(label_encoder_file)
label_encoder_file.close()
classes = label_encoder.classes_

x =[]

input_matrix = np.genfromtxt(test_file, delimiter='\t')
x.append(input_matrix)
input_matrix = adjust_input_to_fixed_length(max_length, x)

x = np.array(x)

number_of_predicted_class= model.predict_classes(x)
class_name = classes[number_of_predicted_class]
print 'moim zdaniem to slowo ' + class_name[0]